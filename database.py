import mysql.connector
from netzwerk_main import Switch, L3Switch, Router


def connect():
   cnx = mysql.connector.connect(
       host="localhost",
       user="root",
       password="",
       database="sdn_it9g"
   )
   return cnx


# Test Function to view table content
def getfulltable(table):

    cnx = connect()
    cursor = cnx.cursor()

    query = ("SELECT * FROM " + table + ";")
    cursor.execute(query)
    result = cursor.fetchall()

    cursor.close()
    cnx.close()

    return result


# Fetches all relevant data for device from database
def getfulldevice(nid):

    cnx = connect()
    cursor = cnx.cursor()

    query = ("SELECT N_ID, n.Bezeichnung, Os_version, t.Bezeichnung, p.Nummer, p.Status, MAC, p.Mode, v.Nummer, v.Bezeichnung,i.Adresse FROM netzwerkgeraet n JOIN typ t ON n.Typ = t.T_ID JOIN port p ON n.N_ID = p.Geraet LEFT JOIN zw_port_vlan zw ON zw.P_ID = p.P_ID LEFT JOIN vlan v ON zw.V_ID = v.Nummer LEFT JOIN ipadresse i ON p.P_ID = i.Port WHERE n.N_ID = " + str(nid) + " ORDER BY p.Nummer;")
    cursor.execute(query)
    result = cursor.fetchall()
    cursor.close()
    cnx.close()

    return result


# Counts number of ports on network device for object creation
def getportnumer(nid):
    cnx = connect()
    cursor = cnx.cursor()

    query = ("SELECT count(*) FROM port WHERE Geraet = " + str(nid) + ";")
    cursor.execute(query)
    result = cursor.fetchall()

    cursor.close()
    cnx.close()

    return result


# Creates a network device from the database
def getnetzwerkgeraet(nid):

    result = getfulldevice(nid)
    portnumber = int(getportnumer(nid)[0][0])

    print(result)

    if result[0][3] == "Switch":
        nwgeraet = Switch(result[0][1], result[0][2], portnumber)
    elif result[0][3] == "L3-Switch":
        nwgeraet = L3Switch(result[0][1], result[0][2], portnumber)
        # print(result[0])
        # Parameter befüllen!!!
        checkdoubles = []
        i = 0
        for entry in result:
            if entry[4] not in checkdoubles:
                checkdoubles.append(entry[4])
                nwgeraet.getports()[i].setportnr(entry[4])
                nwgeraet.getports()[i].setstatus(entry[5])
                nwgeraet.getports()[i].setmac(entry[6])
                nwgeraet.getports()[i].setmode(entry[7])
            i += 1

        # VLAN-Liste
        # VLAN Assign
        # IP-Adresse

    elif result[0][3] == "Router":
        nwgeraet = Router(result[0][1], result[0][2], portnumber)
        checkdoubles = []
        i = 0
        for entry in result:
            if entry[4] not in checkdoubles:
                checkdoubles.append(entry[4])
                nwgeraet.getports()[i].setportnr(entry[4])
                nwgeraet.getports()[i].setstatus(entry[5])
                nwgeraet.getports()[i].setmac(entry[6])
                nwgeraet.getports()[i].setmode(entry[7])
            i += 1

    else:
        nwgeraet = ""

    return nwgeraet


# result = getFullTable("netzwerkgeraet")
# for i in result:
#   print(i)


