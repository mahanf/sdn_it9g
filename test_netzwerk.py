import unittest
from netzwerk_main import Netzwerk


class TestNetzwerk(unittest.TestCase):
    def test_creation(self):

        netz2 = Netzwerk()

        self.assertEqual(1, len(netz2.getvlans()), "VLAN Liste enthaelt kein Default VLAN")


if __name__ == '__main__':
    unittest.main()
