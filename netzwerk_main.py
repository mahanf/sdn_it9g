# Klassen für IT9g SDN-Projekt

class Netzwerk:
    def __init__(self):
        self.__devices = []
        self.__vlans = {}
        default_vlan = Vlan(1, "default")
        self.addvlan(default_vlan)

    def adddevice(self, nwg):
        self.__devices.append(nwg)

    def addvlan(self, vlan):
        self.__vlans[str(vlan.getnummer())] = vlan

    def getvlans(self):
        return self.__vlans

    def showdevices(self):
        for device in self.__devices:
            device.info()


class Netzwerkgeraet:
    def __init__(self, bez, operatingsys, portnumber):
        self.__bezeichnung = bez
        self.__operatingsys = operatingsys
        self.__portnumber = portnumber
        self.__ports = []
        for num in range(0, portnumber):
            self.__ports.append(Port(num))

    def getports(self):
        return self.__ports

    def info(self):
        print(str(type(self)) + ": " + self.__bezeichnung + " - " \
              + str(self.__portnumber) + " Ports")
        for port in self.__ports:
            print(str(port.getportnr() + " " + port.getmac()) + " " + port.getstatus() + " " \
                  + port.getmode() + " " + str(port.getvlans()))


class Router(Netzwerkgeraet):
    def __init__(self, bez, os, portnumber):
        super().__init__(bez, os, portnumber)


class Switch(Netzwerkgeraet):
    def __init__(self, bez, os, portnumber):
        super().__init__(bez, os, portnumber)


class L3Switch(Netzwerkgeraet):
    def __init__(self, bez, os, portnumber):
        super().__init__(bez, os, portnumber)


class IpAddress:
    def __init__(self, add, snm):
        self.__address = add
        self.__subnetmask = snm


class Port:
    def __init__(self, portnr):
        self.__portnr = str(portnr)
        self.__status = "down"
        self.__mac = ""
        self.__mode = "Access"
        self.__vlans = set()
        self.__ipAddress: IpAddress

    def setportnr(self, portnr):
        self.__portnr = portnr

    def setstatus(self, status):
        self.__status = status

    def setmac(self, mac):
        self.__mac = mac

    def setmode(self, mode):
        self.__mode = mode

    def setipaddress(self, ipadd):
        self.__ipAddress = ipadd

    def getportnr(self,):
        return self.__portnr

    def getstatus(self):
        return self.__status

    def getmac(self):
        return self.__mac

    def getmode(self):
        return self.__mode

    def getipaddress(self):
        return self.__ipAddress

    def assignvlan(self, vlan):
        # Only one VLAN in Access mode assignable
        if self.__mode == "Access":
            self.__vlans.clear()
            self.__vlans.add(vlan)
        elif self.__mode == "Trunk":
            self.__vlans.add(vlan)

    def getvlans(self):
        return self.__vlans


class Vlan:
    def __init__(self, nummer, bez):
        self.__nummer = nummer
        self.__bezeichnung = bez

    def __lt__(self, other):
        return self.__nummer < other.getnummer()

    def info(self):
        print(str(self.__nummer) + " " + self.__bezeichnung)

    def getnummer(self):
        return self.__nummer


# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    import database

    # Netzwerkgerät mit Parametern wird von Hand erzeugt
    switch1 = L3Switch("Cisco Catalyst 3560", "19.0", 26)

    switch1.getports()[0].setportnr("Fa0/0")
    switch1.getports()[1].setportnr("Fa0/1")
    switch1.getports()[23].setportnr("Fa0/23")
    switch1.getports()[24].setportnr("Gig0/0/0")

    switch1.getports()[0].setmac("AB:CD:12:34")
    switch1.getports()[1].setmac("AB:CD:34:56")
    switch1.getports()[23].setmac("AB:CD:56:78")
    switch1.getports()[24].setmac("AB:CD:78:90")

    switch1.getports()[24].setmode("Trunk")

    switch1.getports()[0].setstatus("Up")
    switch1.getports()[1].setstatus("Up")
    switch1.getports()[24].setstatus("Up")

    netz1 = Netzwerk()

    netz1.addvlan(Vlan(10, "Verkauf"))
    netz1.addvlan(Vlan(20, "Einkauf"))
    netz1.addvlan(Vlan(99, "Admnistration"))
    netz1.addvlan(Vlan(30, "Chef"))

    switch1.getports()[0].assignvlan(netz1.getvlans()["10"])
    switch1.getports()[1].assignvlan(netz1.getvlans()["20"])
    switch1.getports()[23].assignvlan(netz1.getvlans()["99"])
    for i in netz1.getvlans().values():
        switch1.getports()[24].assignvlan(i)

    switch1.getports()[23].setipaddress(IpAddress("192.168.99.10", "255.255.255.0"))
    ##############

    netz1 = Netzwerk()
    netz1.adddevice(switch1)
    # netz1.showDevices()

    print("")
    # Netzwerkgerät über ID aus DB laden
    switch2 = database.getnetzwerkgeraet(1)
    ##############

    netz1.adddevice(switch2)
    # netz1.showDevices()

    print("")
    # Netzwerkgerät über ID aus DB laden
    router1 = database.getnetzwerkgeraet(3)
    ##############

    netz1.adddevice(router1)
    netz1.showdevices()

    # Test Output #
    # for x in sorted(switch1.getPorts()[24].getVlans()):
    #    x.info()
